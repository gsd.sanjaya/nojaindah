-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2023 at 11:49 PM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpmvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int NOT NULL,
  `penulis` varchar(255) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `tulisan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `penulis`, `judul`, `tulisan`) VALUES
(3, 'Linux', 'Belajar PHP MVC', 'Tutorial PHP MVC'),
(4, 'Linux', 'Belajar OOP PHP', 'Tutorial OOP PHP'),
(5, 'Linux', 'Belajar PHP Dasar', 'Tutorial PHP Dasar');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `User_id` int NOT NULL,
  `Username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`User_id`, `Username`, `email`, `Password`) VALUES
(2, 'nama', 'admin@admin.com', '$2y$10$DAWb8GY/GuICggbY8Q0Pl.COyFfwLkzoMu79xSUcKcZ7BqL9EpVEm'),
(3, 'nopi', 'nopi@gmail.com', '$2y$10$LvLI1uihYPXZbOsfix4cq.3X6Wu7nruVmKo6R2wk95Rb.AVzDQDTG'),
(4, 'hello', 'helo@gmail.com', '$2y$10$PVkoSDN2wwilyULZ61VtE.YiTdcUC5uVdq.pdakNIJbNYiX0YRKjy'),
(5, 'nopiii', 'nopiii@gmail.com', '$2y$10$.r7eV/OcUAnFXHDFCwMiTeqjzcFicW4PefusFnf2IRZo5h3szhCJi'),
(6, 'nesa', 'nesa@gmail.com', '$2y$10$.kXMSYQCDMo7wifnb91g3.2IhNOVTCtSRF5W9AbnPY95xcJLsjvH.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`User_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `User_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
