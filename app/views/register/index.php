<div class="container my-5 col-lg-5">
    <div class="d-flex justify-content-center">
        <div class="col-xl-10 col-lg-12">
            <div class="card border-0 shadow-lg">
                <div class="card-bodyp-0 p-5">
                    <div class="row">


                        <div class="mb-4 text-center">
                            <h2>Wellcome Back!</h2>
                            <!-- <p class="text-secondary">Please Sign In to continue</p> -->
                        </div>
                        <form action="<?= BASEURL; ?>/register/prosesRegister" method="post">
                            <div class="form-group mb-3">
                                <label for="" class="mb-2">Username</label>
                                <input type="text" class="form-control" placeholder="username" name="username">
                            </div>
                            <div class="form-group mb-3">
                                <label for="" class="mb-2">Email</label>
                                <input type="text" class="form-control" placeholder="email" name="email">
                            </div>
                            <div class="form-group mb-3">
                                <label for="" class="mb-2">Password</label>
                                <input type="password" class="form-control" placeholder="password" name="password">
                            </div>
                            <div class="form-group mb-3">
                                <label for="" class="mb-2">Confirm Password</label>
                                <input type="password" class="form-control" placeholder="Confirm Password" name="cpassword">
                            </div>


                            <div class="text-center">
                                <button type="submit" class="btn bg-primary text-light mb-3" style="width: 100%;">Sign Up</button>
                                <p class="text-secondary">Have an Account? <a href="<?= BASEURL; ?>/login" class="text-decoration-none">Sign in</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>