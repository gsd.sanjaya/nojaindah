<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman <?= $data['judul']; ?></title>
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.css">
</head>

<body class="bg-dark">
    <nav class="navbar navbar-expand-lg " style="height: 85px;" >
        <div class="container d-flex justify-content-between">
            <a class=" navbar-brand text-light fw-bold " href="<?= BASEURL; ?>/home">NOJA INDAH</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon "></span>
            </button>
            <div class="" id="navbarNav">
                <div class="navbar-nav"> <a class="nav-item nav-link text-light active" href="<?= BASEURL; ?>">Home</a>
                    <a class="nav-item nav-link text-light" href="<?= BASEURL; ?>/blog">Blog</a>
                    <a class="nav-item nav-link  text-light active" href="<?= BASEURL; ?>/about">About</a>
                </div>
            </div>
            <div class="button-login">
            <?php if (isset($_SESSION['login'])) : ?>
                        <button type="login" class="btn btn-light bg-transparent" id="login-button"><a href="<?= BASEURL; ?>/login/logout" class="text-white text-decoration-none">Logout</a></button>
                    <?php else : ?>
                        <button type="logout" class="btn btn-light bg-transparent" id="logout-button"><a href="<?= BASEURL; ?>/login" class="text-white text-decoration-none">Login</a></button>
                    <?php endif; ?>

            </div>
        </div>
    </nav>