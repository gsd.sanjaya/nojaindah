<!-- <div class="container">
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-active"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                </div>
                                <form class="user">
                                    <div class="form-group row">
                                        <label for="" class="col-lg-4">Email</label>
                                        <input type="email" class="form-control form-control-user col-lg-8" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Username">
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-lg-4">Password</label>
                                        <input type="password" class="form-control form-control-user col-lg-8" id="exampleInputPassword" placeholder="Password">
                                    </div>
                                    <a href="index.html" class="btn btn-primary btn-user btn-block">
                                        Login
                                    </a>
                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="forgot-password.html">Forgot Password?</a>
                                </div>
                                <div class="text-center">
                                    <a class="small" href="register.html">Create an Account!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div> -->

<div class="container my-5">
    <div class="d-flex justify-content-center">
        <div class="col-xl-10 col-lg-12">
            <div class="card border-0 shadow-lg ">
                <div class="card-bodyp-0">
                    <div class="row">
                        <div class="col-lg-5" style="background-image: url('<?= BASEURL; ?>/img/img20.jpeg'); background-size:cover; background-position: buttom ;"></div>
                        <div class="col-lg-6 p-5">
                            <div class="m-5">
                                <div class="mb-4 text-center">
                                    <h2>Wellcome Back!</h2>
                                    <p class="text-secondary">Please Sign In to continue</p>
                                </div>
                                <form action="<?= BASEURL; ?>/login/auth" method="post">
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Username</label>
                                        <input type="text" class="form-control" placeholder="username" name="username">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Password</label>
                                        <input type="password" class="form-control" placeholder="password" name="password">
                                    </div>
                                    <div class="row mb-3">
                                        <div class="d-flex justify-content-center">
                                            <div class="col text-end">
                                                <a href="" class="text-decoration-none">Forgot Password</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button class="btn bg-primary text-light mb-3" style="width: 100%;">Sign In</button>
                                        <p class="text-secondary">New user? <a href="<?= BASEURL; ?>/register" class="text-decoration-none">Sign Up</a></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>